/*Copyright (C) 
 *
 * 2021 - fjrg76 at gmail dot com 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/*
 * timers_crtp_2.ino
 *
 * Igual que timers_crtp_1.ino pero eliminé el método .Exec() de la clase base
 */


#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>


enum class TimerMode : UBaseType_t{ ONE_SHOT, AUTO_RELOAD };


template<typename T>
class TimerX
{
public:

   void Start()
   {
      xTimerStart( 
         this->handler, // timer's handler
         0 );           // blocktime
   }

   size_t Get_ID() const
   {
      return this->id;
   }

protected:

   TimerX( 
      TickType_t   period, 
      TimerMode    mode = TimerMode::ONE_SHOT,
      const char*  name = "",
      size_t       id = 0 
   ) : id{ id }
   {
      this->handler = 
         xTimerCreate(
            name,                              // timer's name
            pdMS_TO_TICKS( period ),           // timer's period
            static_cast<UBaseType_t>( mode ),  // timer's mode
            this,                              // working object
            callback );                        // timer's callback code
   }

   // FreeRTOS registers and calls this function every time the timer's time expires:
   static void callback( TimerHandle_t handler )
   {
      TimerX* p = static_cast<TimerX*>( pvTimerGetTimerID( handler ) );

      static_cast<T&>( *p ).Run();
   }

   TimerHandle_t handler{ nullptr };

   size_t        id{ 0 };
   // the id data member helps to find out which timer calls the user's code
   // (aka "the callback"), as originally intended in FreeRTOS.
   // You can ommit it if not used.
};


class MyTimer : public TimerX<MyTimer>
{
public:
   MyTimer( 
      uint8_t      pin,
      TickType_t   period, 
      TimerMode    mode = TimerMode::ONE_SHOT,
      const char*  name = "",
      size_t       id = 0 
   ) : TimerX{ period, mode, name, id },
       pin{ pin }
   {
      configASSERT( this->handler );
      pinMode( this->pin, OUTPUT );
   }

   void Run()
   {
      state = !state;
      digitalWrite( this->pin, state );

      Serial.print( "ID: " );
      Serial.print( Get_ID() );
      Serial.print( "\n" );
   }

private:
   uint8_t pin{ 0 };
   bool    state{ false };
};


class OtherTimer : public TimerX<OtherTimer>
{
public:
   OtherTimer( 
      const char*  message,
      TickType_t   period, 
      TimerMode    mode = TimerMode::ONE_SHOT,
      const char*  name = "",
      size_t       id = 0 
   ) : TimerX{ period, mode, name, id },
       message{ message }
   {
      configASSERT( this->handler );

      // nothing else yet
   }

   void Run()
   {
      Serial.print( "ID: " );
      Serial.print( this->id );
      Serial.print( ": " );
      Serial.println( this->message );
   }

private:
   const char* message;
};

void setup()
{
   MyTimer my_timer( 13, pdMS_TO_TICKS( 500 ), TimerMode::AUTO_RELOAD, "TMR1", 1 );

   OtherTimer other_timer( "HOLA MUNDO", pdMS_TO_TICKS( 1000 ), TimerMode::AUTO_RELOAD, "TMR2", 2 );

   MyTimer yet_other_timer( 2, pdMS_TO_TICKS( 2000 ), TimerMode::AUTO_RELOAD, "TMR3", 3 );

   my_timer.Start();

   other_timer.Start();

   yet_other_timer.Start();

   Serial.begin( 115200 );

   vTaskStartScheduler();


}

void loop() 
{
   // put your main code here, to run repeatedly:
}
